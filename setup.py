# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

setup(
    name='sample',
    version='1.0',
    description='Simple app using pythons turtle,
    long_description=readme,
    author='David Caulfield',
    author_email='djcfield@hotmail.com',
    url='https://github.com/dcfield/circle_of_squares',
    packages=find_packages()
)
