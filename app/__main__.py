import turtle
my_turtle = turtle.Turtle()

def square(length, angle):
    my_turtle.forward(length)
    my_turtle.left(angle)
    my_turtle.forward(length)
    my_turtle.left(angle)
    my_turtle.forward(length)
    my_turtle.left(angle)
    my_turtle.forward(length)

for a in range(60):
    square(0, 27)
    square(100, 90)
